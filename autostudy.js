// ==UserScript==
// @name         云学堂
// @namespace    https://greasyfork.org/zh-CN/users/41249-tantiancai
// @version      0.31
// @description  云学堂自动挂机学习
// @author       Tantiancai
// @match        http://edu.linkstec.com/package/*
// @grant        none
// ==/UserScript==
(function () {
    'use strict';
    function findStudyPlan() {
        var a = $('#divTaskCenter:first').find('a:first');
        console.log(a.length);
        if (a.length == 1) {
            console.log("click a")
            $(a)[0].click();
        }
    }

    function findStudyMovie() {
        var movie = $('#tbodyTrainInfo.hand td span')
    }
    function getUnsafeWindow() {
        if (this) {
            console.log(this);
            if (typeof(this.unsafeWindow) !== "undefined") { //Greasemonkey, Scriptish, Tampermonkey, etc.
                return this.unsafeWindow;
            } else if (typeof(unsafeWindow) !== "undefined" && this === window && unsafeWindow === window) { //Google Chrome natively
                var node = document.createElement("div");
                node.setAttribute("onclick", "return window;");
                return node.onclick();
            } else {}
        } else { //Opera, IE7Pro, etc.
            return window;
        }
    }

    function GotoNextPage() {
        //存在左侧大纲
        if ($('#divCourseTree').length > 0) {
            var link = $('#divCourseTree').find('.selectednode').parent().next().find('a');
            if (link.length > 0) {
                link[0].click();
                return;
            }
        }

        if ($('dd.active').next().find('a').length > 0) {
            $('dd.active').next().find('a')[0].click();
        } else {
            console.log('学习完成！');
            //GoBack();
        }
    }

    function TimeProcess() {
        findStudyPlan();

        if (typeof(existsUserKnowledge) == 'undefined') {
            clearInterval(processTimer);
            GotoNextPage();
            return;
        }

        if (typeof(GetCurSchedule) != 'undefined') {
            $("input.btnok").click();
            console.log('实际学习时间：' + actualStudyHours + 's 共需学习时间：' + standardStudyHours + 's');

            //$('#divTaskCenter')
            if (!complete && myPlayer.getState() == 'complete') {
                complete = true;
                console.log('open new tab')
                window.open('http://zhcf.yunxuetang.cn/sty/index.htm');
                window.close();
            }

        } else {
            clearInterval(processTimer);
            GotoNextPage();
            return;
        }

        if (blFirst) {
            if (typeof(timecheck) != 'undefined') {
                clearInterval(timecheck);
            }

            if (typeof(myPlayer) != 'undefined' && typeof(actualStudyHours) != 'undefined') {
                if (actualStudyHours > 0) {
                    myPlayer.stop();
                    blFirst = false;
                }
            }
        }
    }

    var myUnsafeWindow = getUnsafeWindow();
    var processTimer = null;
    var blFirst = false;
    var complete = false;
    $(myUnsafeWindow.document).ready(function () {

        console.log('云学堂挂机脚本准备就绪。');
        processTimer = setInterval(TimeProcess, 5000);
    });

})();
