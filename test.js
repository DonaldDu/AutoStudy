function findStudyPlan() {
    var a = $('#divTaskCenter:first').find('a:first');
    if (a.length == 1) {
        console.log("click a ");
        var url = "http://zhcf.yunxuetang.cn" + a.attr('href');
        window.open(url, '_self');
    }
}
function findCourse() {
    var courses = $('#knowledgestudy').find('tr.hand');
    console.log('courses.length ' + courses.length);

    //\/plan/(package|video)/b9f0917040964be9bd5257d4b3d3f2b0_e292d1dfb4e24ad2861c48cc710eec40.html
    var urlReg = /\/plan\/\w+\/[\d\w]+_[\d\w]+\.html/;
    var header = 'https://zhcf.yunxuetang.cn/kng';
    var urls = [];

    courses.each(function () {
        var tr = $(this);
        var percent = tr.find('span:last').text();

        if (percent != '100%') {
            var outerHTML = tr.prop("outerHTML");
            var res = urlReg.exec(outerHTML);
            if (res != null) {
                var url = header + res[0];
                urls.push(url);
                console.log(percent + ' => ' + url);
            }
        }
    });
    if (urls.length > 0 && !isStudying()) {
        window.open(urls[0], '_self');
    }
}

function startStudy() {
    var divcourselist = $('#divcourselist');
    console.log('course list length ' + divcourselist.length);
    if (divcourselist.length == 1) {
        var outerHTML = divcourselist.prop("outerHTML");
        // /package/video/e4351d3116c04f89a2dd88c9d6c6a628_f7566b430bc84704b3f4e930e555f44b.html
        var urlReg = /\/package\/video\/[\d\w]+_[\d\w]+\.html/;
        var res = urlReg.exec(outerHTML);
        if (res != null) {
            var header = 'https://zhcf.yunxuetang.cn/kng/course/'; //跳转到HTTPS视频播放页面
            var url = header + res[0];
            window.open(url, '_self');
        }
    }
}

function isStudying() {
    return localStorage.getItem("isStudying") == true;
}
function getUnsafeWindow() {
    if (this) {
        console.log(this);
        if (typeof(this.unsafeWindow) !== "undefined") { //Greasemonkey, Scriptish, Tampermonkey, etc.
            return this.unsafeWindow;
        } else if (typeof(unsafeWindow) !== "undefined" && this === window && unsafeWindow === window) { //Google Chrome natively
            var node = document.createElement("div");
            node.setAttribute("onclick", "return window;");
            return node.onclick();
        } else {}
    } else { //Opera, IE7Pro, etc.
        return window;
    }
}

function GotoNextPage() {
    //存在左侧大纲
    if ($('#divCourseTree').length > 0) {
        var link = $('#divCourseTree').find('.selectednode').parent().next().find('a');
        if (link.length > 0) {
            link[0].click();
            return;
        }
    }

    if ($('dd.active').next().find('a').length > 0) {
        $('dd.active').next().find('a')[0].click();
    } else {
        console.log('学习完成！');
    }
}

function TimeProcess() {
    if (!isPlaying()) {
        findStudyPlan();
    }

    if (!isPlaying()) {
        findCourse();
    }

    if (!isPlaying()) {
        startStudy();
    }

    if (typeof(existsUserKnowledge) == 'undefined') {
        clearInterval(processTimer);
        GotoNextPage();
        return;
    }

    if (typeof(GetCurSchedule) != 'undefined') {
        $("input.btnok").click();
        console.log('实际学习时间：' + actualStudyHours + 's 共需学习时间：' + standardStudyHours + 's');

        if (isPlayCompleted()) {
            complete = true;
            console.log('open new tab')
            window.open('http://zhcf.yunxuetang.cn/sty/index.htm', '_self');
            localStorage.setItem("isPlaying", false);
        } else {
            localStorage.setItem("isPlaying", true);
        }

    } else {
        clearInterval(processTimer);
        GotoNextPage();
        return;
    }

    if (blFirst) {
        if (typeof(timecheck) != 'undefined') {
            clearInterval(timecheck);
        }

        if (typeof(myPlayer) != 'undefined' && typeof(actualStudyHours) != 'undefined') {
            if (actualStudyHours > 0) {
                myPlayer.stop();
                blFirst = false;
            }
        }
    }
}
function isPlaying() {
    return localStorage.getItem("isPlaying") == 'true';
}
function isPlayCompleted() {
    var percent = $('#ScheduleText').text();
    console.log('percent ' + percent);
    localStorage.setItem("percent", percent);
    return percent == '100%';
    //return !complete && myPlayer.getState() == 'complete'
}
console.log('云学堂挂机脚本debug');

var processTimer = null;
var blFirst = false;
var complete = false;

//var myUnsafeWindow = getUnsafeWindow();
//$(myUnsafeWindow.document).ready(function () {
//
//    console.log('云学堂挂机脚本准备就绪。');
//    processTimer = setInterval(TimeProcess, 5000);
//});
processTimer = setInterval(TimeProcess, 3000);
